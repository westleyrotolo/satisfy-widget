# Satisfy Widget

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14

## Come inserirlo in un sito internet

```js
<script type="text/javascript">
  var tag = document.createElement('app-widget');
  tag.setAttribute('data-entrypoints', 'abcdefgh-1234-5678-abcdefghujk');
  document.body.appendChild(tag); 
  var script_tag = document.createElement('script');
  script_tag.setAttribute('src','https://satisfy.opencontent.it/widget.js');
  document.head.appendChild(script_tag);
</script>
```

## LocalStorage

L'applicazione controlla all'avvio se giè presente dati di un precedente invio, se non li ha oppure inviati da un arco di tempo definito mostra il widget o meno.
Per impedire l'invio di molteplici valori per uno stesso servizio in un intervallo di tempo ristretto creiamo nel localStorage dei dati di verifica.
Ad ogni invio viene creato nel localStorage una chiave con questi valori

MD5 - Name | Name | Cosa contiene | Durata
123 | satisfy-{entrypoints_id}-{location-path} | val stelletta | 24 ore

| MD5 - Name |                 Nome key                 |    Cosa contiene | Durata |
| ---------- | :--------------------------------------: | ---------------: |-------:|
| 123        | satisfy-{entrypoints_id}-{location-path} | valore stelletta | 24 ore |

Alla scadenza delle 24 ore viene cancellato in automatico la chiave e il valore nel localStorage

## Developers

Per testare la build in locale lanciare dal browser il file test.html
Verificare all'interno del file che lo script punti alla build da testare.

## Deploy

La build del widget può essere fatta in modalità DEV, PROD oppure con stili e senza stili bootstrap-italia

```
    ## Build DEV con stili
    "build:elements:dev": "ng build --configuration development --output-hashing none --single-bundle && node -r dotenv/config build-script.js dotenv_config_path=.env.dev",
   
    ## Build PROD con stili
    "build:elements:production": "ng build --configuration production --output-hashing none --single-bundle && node -r dotenv/config build-script.js dotenv_config_path=.env",
    
    ## Build PROD senza stili
    "build:elements:production-ns": "ng build --configuration production-ns --output-hashing none --single-bundle && node -r dotenv/config build-script.js dotenv_config_path=.env.ns",
    
    ## Build tutte le tre precedenti con un solo comando
    "build:all": "npm run build:elements:dev && npm run build:elements:production && npm run build:elements:production-ns",

```

Dopo la build viene generato un file JS nella cartella "elements" contenenti tutti le build precedenti e correnti "widget-{version}-dev.js" "widget.js" "widget_ns.js"

## Maggiori informazioni

Per maggiori info consulta la [pagina del progetto](https://gitlab.com/opencontent/satisfy)
