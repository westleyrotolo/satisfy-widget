FROM node:16-alpine

ARG ${ENV:-PROD}
ARG ${NG_ENV:-production}

WORKDIR /srv

RUN npm install -g @angular/cli

COPY package.json .
RUN npm config set legacy-peer-deps true
RUN npm install

ADD . .
RUN ng build --configuration $NG_ENV --output-hashing none --single-bundle --optimization
RUN node build-script.js
RUN ls -l elements

#FROM amazon/aws-cli
#
#COPY --from=0 /srv/elements /srv
#
#ARG AWS_ACCESS_KEY_ID
#ARG AWS_SECRET_ACCESS_KEY
#RUN aws s3 sync /srv s3://satisfy.opencontent.it/
