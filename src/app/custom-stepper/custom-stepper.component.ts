import { CdkStepper } from '@angular/cdk/stepper';
import { Component, EventEmitter, Input, Output } from '@angular/core';

/** Custom CDK stepper component */
@Component({
  selector: 'custom-stepper',
  templateUrl: './custom-stepper.component.html',
  styleUrls: ['./custom-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: CustomStepper }],
})
export class CustomStepper extends CdkStepper {
  @Output()
  submitted = new EventEmitter<any>();
  disabled: boolean = true;

  @Input()
  set enableNextStep(val: boolean) {
    this.disabled = val;
  }

  @Input()
  md5Path!: string | null;

  selectStepByIndex(index: number): void {
    this.selectedIndex = index;
  }

  OnchangeStep($event: MouseEvent, selectedIndex: any, send? : boolean) {
    if (send) {
      this.submitted.emit(true);
    }
  }
}
