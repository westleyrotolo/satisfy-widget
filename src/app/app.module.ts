import { Injector, NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { createCustomElement } from '@angular/elements';
import { WidgetComponent } from './widget/widget.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { LocalStorageService } from './local-storage.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { CdkStepper, CdkStepperModule } from '@angular/cdk/stepper';
import { StepOneComponent } from './step-one/step-one.component';
import { ApiService } from './api.service';
import { StepTwoComponent } from './step-two/step-two.component';
import { CustomStepper } from './custom-stepper/custom-stepper.component';
import { OptionItemComponent } from './option-item/option-item.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

@NgModule({
  declarations: [
    WidgetComponent,
    StepOneComponent,
    StepTwoComponent,
    CustomStepper,
    OptionItemComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    GraphQLModule,
    CdkStepperModule,
    ReactiveFormsModule,
    FormsModule,
    // ngx-translate and the loader module
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    CookieService,
    LocalStorageService,
    ApiService,
    { provide: CdkStepper },
  ],
})
export class AppModule {

  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const el = createCustomElement(WidgetComponent, {
      injector: this.injector,
    });
    customElements.define('app-widget', el);
  }
}


export function createTranslateLoader(
  http: HttpClient
): TranslationLoaderService {
  return new TranslationLoaderService(http);
}

export class TranslationLoaderService implements TranslateLoader {
  constructor(http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    if (lang === 'en') {
      return of({
        rating: {
          title: 'Rate the page from 1 to 5 stars',
          value_1: 'Rate 1 out of 5 stars',
          value_2: 'Rate 2 out of 5 stars',
          value_3: 'Rate 3 out of 5 stars',
          value_4: 'Rate 4 out of 5 stars',
          value_5: 'Rate 5 out of 5 stars',
        },
        stepper: { back: 'Back', next: 'Come in', send: 'Send' },
        form: {
          select_options: 'Choose at least one option',
          other_details: 'Want to add more details?',
          detail: 'Detail',
          max_chars: 'Please enter a maximum of 200 characters',
        },
      });
    } else if (lang === 'de') {
      return of({
        rating: {
          title: 'Bewerten Sie die Seite von 1 bis 5 Sternen',
          value_1: 'Bewertung 1 von 5 Sternen',
          value_2: 'Bewertung 2 von 5 Sternen',
          value_3: 'Bewertung 3 von 5 Sternen',
          value_4: 'Bewertung 4 von 5 Sternen',
          value_5: 'Bewertung 5 von 5 Sternen',
        },
        stepper: {
          back: 'Zurück',
          next: 'Herein',
          send: 'Senden',
        },
        form: {
          select_options: 'Wählen Sie mindestens eine Option',
          other_details: 'Möchten Sie weitere Details hinzufügen?',
          detail: 'Detail',
          max_chars: 'Bitte geben Sie maximal 200 Zeichen ein',
        },
      });
    } else {
      return of({
        rating: {
          title: 'Valuta da 1 a 5 stelle la pagina',
          value_1: 'Valuta 1 stelle su 5',
          value_2: 'Valuta 2 stelle su 5',
          value_3: 'Valuta 3 stelle su 5',
          value_4: 'Valuta 4 stelle su 5',
          value_5: 'Valuta 5 stelle su 5',
        },
        stepper: {
          back: 'Indietro',
          next: 'Avanti',
          send: 'Invia',
        },
        form: {
          select_options: 'Scegli almeno un’opzione',
          other_details: 'Vuoi aggiungere altri dettagli?',
          detail: 'Dettaglio',
          max_chars: 'Inserire massimo 200 caratteri',
        },
      });
    }
  }
}
