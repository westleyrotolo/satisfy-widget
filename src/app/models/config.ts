export interface Message {
  language: string;
  question: string | null;
  thanks_negative: string;
  thanks_positive: string;
}

export interface Entrypoint {
  id: string;
  type: string;
  messages: Message[];
  questions: Questions[];
  is_review_enable: boolean;
  rating_positive_min: number;
}

export interface Config {
  entrypoints: Entrypoint[];
}

export interface Question {
  is_positive: boolean;
  id: string;
  is_shared: boolean;
  translations: Translation[];
  answers: Answer[];
}

export interface Questions {
  question: Question;
}

export interface Answer {
  id: string;
  priority: number;
  translations: Translation[];
}

export interface Translation {
  language: string;
  title: string;
}
