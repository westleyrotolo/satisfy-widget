import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-option-item',
  templateUrl: './option-item.component.html',
  styleUrls: ['./option-item.component.scss'],
})
export class OptionItemComponent implements OnInit {
  @Input() label!: string;
  @Input() ctrl!: FormControl;
  @Input() value!: string;
  @Input() id!: string;
  @Input() checked!: string;

  constructor() {}

  ngOnInit(): void {}

}
