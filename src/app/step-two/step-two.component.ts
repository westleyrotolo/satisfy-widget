import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss'],
})
export class StepTwoComponent implements OnInit {
  public stepTwoForm: FormGroup;

  @Input() selectedStep: number = 2;
  @Input() stepsLength: number = 2;

  @Input()
  md5Path!: string | null;

  @ViewChild('detailInput') detailInput!: ElementRef;
  @ViewChild('detailInputLabel') detailInputLabel!: ElementRef;


  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    private renderer: Renderer2
  ) {
    this.stepTwoForm = this.fb.group({
      details: [''],
    });
  }

  ngOnInit(): void {
    this.stepTwoForm.valueChanges.subscribe(selectedValue => {

      let localData = JSON.parse(
        // @ts-ignore
        this.localStorageService.getData(`${this.md5Path}`)
      );
      let item = {
        ...localData,
        review: selectedValue.details,
      };
      this.localStorageService.saveData(
        `${this.md5Path}`,
        JSON.stringify(item)
      );
    });
  }


  onFocusIn(): void {
    this.renderer.setAttribute(this.detailInput.nativeElement, 'data-focus-mouse', 'true');
    this.addClass('focus--mouse',this.detailInput.nativeElement)
    this.addClass('active',this.detailInputLabel.nativeElement)

  }
  onFocusOut(): void {
    this.renderer.setAttribute(this.detailInput.nativeElement, 'data-focus-mouse', 'false');
    this.removeClass('focus--mouse',this.detailInput.nativeElement)
    if(!this.detailInput.nativeElement.value){
      this.removeClass('active',this.detailInputLabel.nativeElement)
    }
  }

  addClass(className: string, element: any) {
    this.renderer.addClass(element, className);
  }

  removeClass(className: string, element: any) {
    this.renderer.removeClass(element, className);
  }
}
