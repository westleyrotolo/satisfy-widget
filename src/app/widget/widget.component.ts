import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { ApiService } from '../api.service';
import { Config, Message, Questions } from "../models/config";
import FP from '@fingerprintjs/fingerprintjs';
import { Subject } from 'rxjs';
import { Md5 } from 'ts-md5';
import { LocalStorageService } from '../local-storage.service';
import * as WebFont from 'webfontloader';
import { takeUntil } from 'rxjs/operators';
import packageInfo from 'package.json';
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../environments/environment";
import { loadFonts } from 'bootstrap-italia';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: [environment.style ==  1 ? './widget.component.scss' : './widget-ns.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WidgetComponent implements OnInit, OnDestroy {

  // Init variables
  disabled = false;
  language: string = 'it_IT'; // Language Browser
  config!: Config;
  loading = true;
  id: string = ''; // Fingerprint
  entrypoint_id!: string;
  private data: any;
  private unsubscribe$ = new Subject<void>();
  entrypoints: string | null;
  messages: Message[] = [];
  rating_positive_min = 1;
  md5Path: string | null;
  rateValue = 0; // Default value ratings
  questions: Questions[] = [];
  thanksMessage: string = '';
  positive_message: any;
  negative_message: any;
  show_questions = false;
  isPositive = false;
  private error: any;
  public version: string = packageInfo.version;

  // Disabled Next step by default
  enableNextStep = true

  submitted = false;

  constructor(
    private ref: ChangeDetectorRef,
    private apiService: ApiService,
    private elementRef: ElementRef,
    private localStorageService: LocalStorageService,
    private translate: TranslateService
  ) {
    this.entrypoints =
      this.elementRef.nativeElement.getAttribute('data-entrypoints');
    this.md5Path = Md5.hashStr(
      `satisfy-${this.entrypoints}-${window.location.href}`
    );

    translate.setDefaultLang('it');
    translate.use(document.documentElement.lang ? document.documentElement.lang:  'it');
  }

  ngOnInit(): void {
    if(environment.style ==  1 ){
      // Load web font
      loadFonts('https://static.opencityitalia.it/fonts');
    }
    this.getLanguageBrowser();
    if (!this.localStorageService.getData(`${this.md5Path}`)) {
      this.loadWidget();
    } else {
      let jsonItem
      if(this.localStorageService.getData(`${this.md5Path}`) && localStorage[`${this.md5Path}`]){
        let t = this.localStorageService.getData(`${this.md5Path}`)
         // @ts-ignore
        jsonItem = JSON.parse(t.toString())
      }

      const now = new Date();

      if (now.getTime() > jsonItem.expiry) {
        localStorage.removeItem(`${this.md5Path}`);
        this.loadWidget();
        return;
      } else {
        //Commented for first submit
        this.disabled = true;
        this.show_questions = false;
        this.submitted = true
        this.rateValue = jsonItem.value
        this.loadWidget();
      }
    }
  }

  sendFeedback(rate: any) {
    this.rateValue = rate
    this.calculateStateRating(rate);


    // Create Payload POST
    this.data = {
      version: 1,
      'widget-version': this.version,
      id: Md5.hashStr(this.id + window.location.href), //MD5
      value: rate,
      timestamp: new Date().toISOString(),
      'entrypoint-id': this.entrypoint_id,
      meta: {
        uri: window.location.href,
        title: document.title,
        'user-agent': window.navigator.userAgent,
        lang: window.navigator.language,
      },
    };

    let dateExpired = new Date();
    // Imposto la scadenza a 24 ore
    dateExpired.setTime(dateExpired.getTime() + (24 * 60 * 60 * 1000));
    const item = {
      ...this.data,
      'entrypoint-id': this.entrypoint_id,
      expiry: dateExpired.setTime(
        dateExpired.getTime() + (24 * 60 * 60 * 1000)
      ),
    };
    this.localStorageService.saveData(`${this.md5Path}`,JSON.stringify(item))

    /* Commented logic first submit on click star
    this.disabled = true;
    this.apiService
      .sendFeedBack(this.data)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.loading = false;
          let dateExpired = new Date();
          // Imposto la scadenza a 24 ore
          dateExpired.setTime(dateExpired.getTime() + (24 * 60 * 60 * 1000));
          const item = {
            ...this.data,
            'entrypoint-id': this.entrypoint_id,
            expiry: dateExpired.setTime(
              dateExpired.getTime() + (24 * 60 * 60 * 1000)
            ),
          };
          this.localStorageService.saveData(`${this.md5Path}`,JSON.stringify(item))
        }
      );

     */
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  loadWidget() {
    this.loading = true;
    FP.load({})
      .then(fp => fp.get())
      .then(res => {
        this.id = res.visitorId;
      });

    if (this.entrypoints) {
      this.apiService
        .getConfigGQ(this.entrypoints, this.language)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((result: any) => {
          this.config = result.data || null;
          if(this.config.entrypoints?.length > 0){
            this.entrypoint_id = this.config.entrypoints[0].id;
            this.rating_positive_min =
              this.config.entrypoints[0].rating_positive_min;
            this.messages = this.config.entrypoints[0].messages || [];
            this.positive_message =
              this.config.entrypoints[0].messages[0].thanks_positive;
            this.negative_message =
              this.config.entrypoints[0].messages[0].thanks_negative;
            this.questions = this.config.entrypoints[0].questions || [];
            this.calculateStateRating(this.rateValue)
          }

          this.loading = result.loading;
          this.error = result.error;
        });
    }
    this.ref.detectChanges();
  }

  calculateStateRating(value: number) {
    if(value > 0){
      this.show_questions = true;
      if (value >= this.rating_positive_min) {
        this.thanksMessage = this.messages[0].thanks_positive;
        this.isPositive = true;
        this.questions = this.config.entrypoints[0]?.questions.length
          ? this.config.entrypoints[0]?.questions.filter(
            el => el.question.is_positive
          )
          : [];
      } else {
        this.thanksMessage = this.messages[0].thanks_negative;
        this.isPositive = false;
        this.questions = this.config.entrypoints[0]?.questions.length
          ? this.config.entrypoints[0]?.questions.filter(
            el => !el.question.is_positive
          )
          : [];
      }
    }
  }

  getLanguageBrowser() {
    let defaultLang = 'it_IT'
    if(document.documentElement.lang){
       defaultLang = document.documentElement.lang + '_' + document.documentElement.lang.toUpperCase();
    }
      this.language = defaultLang
  }

  submit() {
   // this.submitted = true
    let localData = JSON.parse(
      // @ts-ignore
      this.localStorageService.getData(`${this.md5Path}`)
    );
    let item = {
      ...localData,
    };
    this.apiService
      .sendFeedBack(item)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.messages[0].question = this.thanksMessage
          this.submitted = true
          this.show_questions = false
          this.disabled = true;
        })
  }

  detectEnableNextStep(value: boolean) {
    this.enableNextStep = value
  }


}
