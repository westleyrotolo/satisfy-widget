export const environment = {
  production: false,
  api: 'https://satisfy.opencityitalia.it/api/v1/ratings',
  api_config_rest: 'https://satisfy.opencityitalia.it/api/rest',
  api_config_gh: 'https://satisfy.opencityitalia.it/v1/graphql',
  style: 1
};
