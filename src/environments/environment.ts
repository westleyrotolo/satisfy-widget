export const environment = {
  production: false,
  api: 'https://satisfy.boat.opencontent.io/api/v1/feedbacks',
  api_config_rest: 'https://satisfy.opencityitalia.it/api/rest',
  api_config_gh: 'https://satisfy.opencityitalia.it/v1/graphql',
  style: 1
};
